package com.biblezon.DailyActivitiesAdult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;

import java.io.File;

/**
 * Created by Shruti on 12/28/2015.
 */
public class SplashActivity extends Activity {

    RelativeLayout splash_rl;
    Animation zoom_in;
    Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        initViews();
    }

    private void initViews() {
        mActivity = this;
        splash_rl = (RelativeLayout) findViewById(R.id.splash_rl);
        zoom_in = AnimationUtils.loadAnimation(mActivity, R.anim.zoom_in);
        splash_rl.startAnimation(zoom_in);

        File folder = new File(Environment.getExternalStorageDirectory() + "/WARRANTY_APP");
        if (!folder.exists()) {
            folder.mkdir();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
//                new VersionCheckAPIHandler(mActivity, webAPIResponseLinsener());
                doTaskAfterVersionCheck();
            }
        }, 3000);

    }

    private void doTaskAfterVersionCheck() {
        if (SessionManager.getInstance(mActivity).isLoggedIn()) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }
    }

    /**
     * Mass Base Response Listener
     */
    private WebAPIResponseListener webAPIResponseLinsener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @Override
            public void onSuccessOfResponse(Object... arguments) {
                doTaskAfterVersionCheck();
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                doTaskAfterVersionCheck();
            }
        };
        return mListener;
    }
}
