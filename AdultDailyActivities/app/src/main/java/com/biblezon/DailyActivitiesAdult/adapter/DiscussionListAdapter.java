package com.biblezon.DailyActivitiesAdult.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.model.DiscussionBaseModel;
import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by sonia on 13/8/15.
 */
public class DiscussionListAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<DiscussionBaseModel> listItems;
    String TAG = DiscussionListAdapter.class.getSimpleName();

    public DiscussionListAdapter(Context context, ArrayList<DiscussionBaseModel> list) {
        mContext = context;
        listItems = new ArrayList<DiscussionBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public DiscussionBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<DiscussionBaseModel> updatedResponseModel) {
        listItems = new ArrayList<DiscussionBaseModel>();
        listItems.addAll(updatedResponseModel);
        Collections.reverse(listItems);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext.getApplicationContext(),
                    R.layout.discussion_list_row, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (listItems != null) {
            final DiscussionBaseModel model = listItems.get(position);
            AndroidAppUtils.showLog(TAG, SessionManager.getInstance(mContext).getUserEmail() + "\n" + model.getEmail());
            /**
             * todays date
             */
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(new Date()); // Now use today date.
            String todaysDate = sdf.format(c.getTime());
            if (SessionManager.getInstance(mContext).getUserEmail().equalsIgnoreCase(model.getEmail())) {
                holder.left_box.setVisibility(View.GONE);
                holder.right_box.setVisibility(View.VISIBLE);
                holder.right_title.setText(model.getComment());
                holder.right_name.setText(model.getFirst_name());
                String date = model.getComment_date();
                if (date.contains(todaysDate)) {
                    date = date.replace(todaysDate, "");
                }
                holder.right_date.setText(date);
            } else {
                holder.left_box.setVisibility(View.VISIBLE);
                holder.right_box.setVisibility(View.GONE);
                holder.left_title.setText(model.getComment());
                holder.left_date.setText(model.getComment_date());
                holder.left_name.setText(model.getFirst_name());
                String date = model.getComment_date();
                if (date.contains(todaysDate)) {
                    date = date.replace(todaysDate, "");
                }
                holder.right_date.setText(date);
            }
        }
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Shruti
     */
    class ViewHolder {
        TextView right_title, right_date, left_title, left_date, left_name, right_name;
        RelativeLayout left_box, right_box;

        public ViewHolder(View view) {
            right_title = (TextView) view
                    .findViewById(R.id.right_title);
            right_date = (TextView) view
                    .findViewById(R.id.right_date);
            left_title = (TextView) view
                    .findViewById(R.id.left_title);
            left_name = (TextView) view.findViewById(R.id.left_name);
            right_name = (TextView) view.findViewById(R.id.right_name);
            left_date = (TextView) view
                    .findViewById(R.id.left_date);
            left_box = (RelativeLayout) view
                    .findViewById(R.id.left_box);
            right_box = (RelativeLayout) view
                    .findViewById(R.id.right_box);

            view.setTag(this);
        }
    }
}
