package com.biblezon.DailyActivitiesAdult.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.adapter.AnnounceListAdapter;
import com.biblezon.DailyActivitiesAdult.control.HeaderViewManager;
import com.biblezon.DailyActivitiesAdult.iHelper.HeaderViewClickListener;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;

import java.util.ArrayList;

import static com.biblezon.DailyActivitiesAdult.fragments.DashboardFragment.mAnnoucementDescrption;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class AnnounceFragment extends Fragment {

    /**
     * ResponseBaseModel Data Model
     */
    public static ResponseBaseModel mBaseModel;
    boolean single = true;
    private String TAG = AnnounceFragment.class.getSimpleName();
    private Activity mActivity;
    private TextView mAnnouncemantData;
    /**
     * Screen base view
     */
    private View mView;
    private AnnounceListAdapter mListAdapter;
    private ArrayList<ResponseBaseModel> mFilteredArrayList;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.announce, container, false);
        initViews();
        manageHeaderOfScreen();
        return mView;
    }

    private HeaderViewClickListener manageHeaderClick() {
        HeaderViewClickListener headerViewClickListener = new HeaderViewClickListener() {
            @Override
            public void onClickOfHeaderLeftView() {

            }

            @Override
            public void onClickOfHeaderRightView() {

            }
        };
        return headerViewClickListener;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        try {
            mAnnouncemantData = (TextView) mView.findViewById(R.id.mAnnouncemantData);

            AndroidAppUtils.showLog(TAG, "Annoucment TextDetails :" + mAnnouncemantData);
            AndroidAppUtils.showLog(TAG, "Annoucment Details :" + mAnnoucementDescrption);
            if (mAnnoucementDescrption == null || mAnnoucementDescrption.isEmpty())
                mAnnoucementDescrption = mActivity.getResources().getString(R.string.nothing);
            mAnnouncemantData.setText((Html.fromHtml(mAnnoucementDescrption).toString()));
        }catch (Exception e){
            e.printStackTrace();

        }
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                manageHeaderClick());
        HeaderViewManager.getInstance().setHeading(true,
                "Announcement");
    }


}
