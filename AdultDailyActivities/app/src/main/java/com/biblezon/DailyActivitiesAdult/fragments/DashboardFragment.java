package com.biblezon.DailyActivitiesAdult.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.MainActivity;
import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.adapter.DashboardListAdapter;
import com.biblezon.DailyActivitiesAdult.control.HeaderViewManager;
import com.biblezon.DailyActivitiesAdult.control.LeftSlidingMenuControl;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;
import com.biblezon.DailyActivitiesAdult.webservices.DashboardBaseAPIHandler;
import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;

import java.util.ArrayList;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class DashboardFragment extends Fragment {

    /**
     * ResponseBaseModel Data Model
     */
    public static ResponseBaseModel mBaseModel;
    ListView todo_listview, suggestion_listview;
    TextView announcement, announcement_title;
    private String TAG = DashboardFragment.class.getSimpleName();
    private Activity mActivity;
    public static String mAnnoucementDescrption = "";
    /**
     * Screen base view
     */
    private View mView;
    private DashboardListAdapter mTODOListAdapter, mSuggestionListAdapter;
    private ArrayList<ResponseBaseModel> mTODOArrayList, mSuggestionArrayList;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.dashboard, container, false);
        initViews();
        assignClicks();
        manageHeaderOfScreen();
        MainActivity.getInstance().showProgressBar(true);
        new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_ADULT, GlobalKeys.DASHBOARD_ADULT_KEY,
                mActivity.getResources().getString(R.string.announcements), AnnouncementApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
        return mView;
    }

    private void assignClicks() {
        announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LeftSlidingMenuControl.getInstance().callNextFragment(-1);
            }
        });
        announcement_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LeftSlidingMenuControl.getInstance().callNextFragment(-1);
            }
        });
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        todo_listview = (ListView) mView.findViewById(R.id.todo_listview);
        suggestion_listview = (ListView) mView.findViewById(R.id.suggestion_listview);
        announcement = (TextView) mView.findViewById(R.id.announcement);
        announcement_title = (TextView) mView.findViewById(R.id.announcement_title);
        todo_listview.setCacheColorHint(Color.TRANSPARENT);
        todo_listview.requestFocus(0);
        mTODOArrayList = new ArrayList<>();
        mSuggestionArrayList = new ArrayList<>();
        mTODOListAdapter = new DashboardListAdapter(mActivity,
                mTODOArrayList);
        mSuggestionListAdapter = new DashboardListAdapter(mActivity, mSuggestionArrayList);
        todo_listview.setAdapter(mTODOListAdapter);
        suggestion_listview.setAdapter(mSuggestionListAdapter);
//        AndroidAppUtils.setListViewHeightBasedOnChildren(todo_listview);
        setListViewHeightBasedOnChildren(todo_listview);
        setListViewHeightBasedOnChildren(suggestion_listview);
    }

    private WebAPIResponseListener AnnouncementApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ArrayList<ResponseBaseModel> responseList = (ArrayList<ResponseBaseModel>) arguments[0];
                    if (responseList != null && responseList.size() > 0) {
                        if (responseList.get(0).getId().equalsIgnoreCase(GlobalKeys.DASHBOARD_ANNOUNCEMENT)) {
                            announcement.setText(responseList.get(0).getTitle());
                            mAnnoucementDescrption = responseList.get(0).getDescription();
                        } else {
                            announcement.setText(mActivity.getResources().getString(R.string.nothing));
                            mAnnoucementDescrption = mActivity.getResources().getString(R.string.nothing);
                        }

                        for (int i = 0; i < responseList.size(); i++) {
                            if (!(responseList.get(i).getId().
                                    equalsIgnoreCase(GlobalKeys.DASHBOARD_ANNOUNCEMENT)) && !(responseList.get(i).getId().
                                    equalsIgnoreCase(GlobalKeys.DASHBOARD_DISCUSSION))) {
                                mTODOArrayList.add(responseList.get(i));
                                if (mTODOArrayList != null
                                        && mTODOArrayList.size() > 0) {
                                    addDataIntoList();
                                }
                            }
                        }
                        if (responseList.get(responseList.size() - 1).getId().equalsIgnoreCase(GlobalKeys.DASHBOARD_DISCUSSION)) {
                            mSuggestionArrayList.add(responseList.get(responseList.size() - 1));
                            if (mSuggestionArrayList != null
                                    && mSuggestionArrayList.size() > 0) {
                                addSuggestionDataIntoList();
                            }
                        }

                        setListViewHeightBasedOnChildren(todo_listview);
                        setListViewHeightBasedOnChildren(suggestion_listview);

                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
            }
        };
        return mListener;
    }

    private WebAPIResponseListener ReflectionApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel reflectionModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(reflectionModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_READING, GlobalKeys.DASHBOARD_READING_KEY,
                        mActivity.getResources().getString(R.string.todays_reading), ReadingApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_READING, GlobalKeys.DASHBOARD_READING_KEY,
                        mActivity.getResources().getString(R.string.todays_reading), ReadingApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }
        };
        return mListener;
    }


    private WebAPIResponseListener ReadingApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel readingModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(readingModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_SAINT_OF_DAY, GlobalKeys.DASHBOARD_SAINT_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.saint_of_the_day), SaintApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_SAINT_OF_DAY, GlobalKeys.DASHBOARD_SAINT_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.saint_of_the_day), SaintApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener SaintApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel saintModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(saintModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_QUIZ_OF_DAY, GlobalKeys.DASHBOARD_QUIZ_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.quiz_of_the_day), QuizApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_QUIZ_OF_DAY, GlobalKeys.DASHBOARD_QUIZ_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.quiz_of_the_day), QuizApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener QuizApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel quizModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(quizModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_DISCUSSION, GlobalKeys.DASHBOARD_DISCUSSION_KEY,
                        mActivity.getResources().getString(R.string.discussion), DiscussionApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_DISCUSSION, GlobalKeys.DASHBOARD_DISCUSSION_KEY,
                        mActivity.getResources().getString(R.string.discussion), DiscussionApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener PrayerApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel prayerModel = (ResponseBaseModel) arguments[0];
                    mSuggestionArrayList.add(prayerModel);
                    if (mSuggestionArrayList != null
                            && mSuggestionArrayList.size() > 0) {
                        addSuggestionDataIntoList();
                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
            }
        };
        return mListener;
    }

    private WebAPIResponseListener DiscussionApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel discussionModel = (ResponseBaseModel) arguments[0];
                    mSuggestionArrayList.add(discussionModel);
                    if (mSuggestionArrayList != null
                            && mSuggestionArrayList.size() > 0) {
                        addSuggestionDataIntoList();
                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
            }
        };
        return mListener;
    }

    private void addDataIntoList() {
        mTODOListAdapter.updateListData(mTODOArrayList);
        mTODOListAdapter.notifyDataSetChanged();
    }

    private void addSuggestionDataIntoList() {
        mSuggestionListAdapter.updateListData(mSuggestionArrayList);
        mSuggestionListAdapter.notifyDataSetChanged();
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                null);
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.dashboard));
    }

    public static void setListViewHeightBasedOnChildren(ListView myListView) {
//        ListAdapter myListAdapter = myListView.getAdapter();
//        if (myListAdapter == null) {
//            //do nothing return null
//            return;
//        }
//        //set listAdapter in loop for getting final size
//        int totalHeight = 0;
//        for (int size = 0; size < myListAdapter.getCount(); size++) {
//            View listItem = myListAdapter.getView(size, null, myListView);
//            listItem.measure(0, 0);
//            totalHeight += listItem.getMeasuredHeight() + 20;
//        }
//        //setting listview item in adapter
//        ViewGroup.LayoutParams params = myListView.getLayoutParams();
//        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
//        myListView.setLayoutParams(params);
//        // print height of adapter on log
//        Log.i("height of listItem:", String.valueOf(totalHeight));
    }
}