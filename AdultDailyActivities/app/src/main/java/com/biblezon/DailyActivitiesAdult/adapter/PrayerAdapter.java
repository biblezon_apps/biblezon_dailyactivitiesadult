package com.biblezon.DailyActivitiesAdult.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;

import java.util.ArrayList;


public class PrayerAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<ResponseBaseModel> listItems;
    String TAG = PrayerAdapter.class.getSimpleName();

    public PrayerAdapter(Context context, ArrayList<ResponseBaseModel> list) {
        mContext = context;
        listItems = new ArrayList<ResponseBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public ResponseBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<ResponseBaseModel> updatedResponseModel, boolean single) {
        if (single) {
            if (updatedResponseModel != null && updatedResponseModel.size() > 0) {
                listItems = new ArrayList<ResponseBaseModel>();
                listItems.add(updatedResponseModel.get(0));
            }
        } else {
            listItems = new ArrayList<ResponseBaseModel>();
            listItems.addAll(updatedResponseModel);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_row, null);
        } else {
            view = convertView;
        }
        ResponseBaseModel responseBaseModel = listItems.get(position);

        if (responseBaseModel != null) {
            try {
                TextView titleView = (TextView) view.findViewById(R.id.tv_title);
                TextView dateView = (TextView) view
                        .findViewById(R.id.date);
                AndroidAppUtils.showLog(TAG, "Prayer Description :" + responseBaseModel.getDescription());
                titleView.setText((Html.fromHtml(responseBaseModel.getDescription()).toString()));
                dateView.setText("Date : " + responseBaseModel.getDate());
                // serialview.setText(responseBaseModel.getActivityID() + ".)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return view;
    }
}
