package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.model.DiscussionBaseModel;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.AppUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class GetDiscussionBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetDiscussionBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<DiscussionBaseModel> mresponseBaseList = new ArrayList<DiscussionBaseModel>();
    private String title, description, date;
    ResponseBaseModel responseBaseModel;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetDiscussionBaseAPIHandler(Activity mActivity,
                                       WebAPIResponseListener webAPIResponseListener) {
//        AndroidAppUtils.showProgressDialog(mActivity, "Loading...", false);
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.DISCUSSION).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response, true);
//                        AndroidAppUtils.hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                AndroidAppUtils.hideProgressDialog();
                try {
                    parseAPIResponse(new JSONObject(ReadFromfile(
                            "discussion.txt", mActivity)), false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getAuthToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.GET_DISCUSSION_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response, boolean online) {
//        if (response != null && WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
        /* Success of API Response */
        try {
//                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
//                for (int i = 0; i < mArrayData.length(); i++) {
            JSONObject mOuterJsonObject = response.getJSONObject(GlobalKeys.RESPONSE_DISCUSSION);
            responseBaseModel = new ResponseBaseModel();
            if (mOuterJsonObject.has(GlobalKeys.RESPONSE_TITLE)) {
                responseBaseModel.setTitle(mOuterJsonObject
                        .getString(GlobalKeys.RESPONSE_TITLE));
            }
            if (mOuterJsonObject.has(GlobalKeys.RESPONSE_DESCRIPTION)) {
                responseBaseModel.setDescription(mOuterJsonObject
                        .getString(GlobalKeys.RESPONSE_DESCRIPTION));
            }
            if (mOuterJsonObject.has(GlobalKeys.DATE)) {
                responseBaseModel.setDate(mOuterJsonObject
                        .getString(GlobalKeys.DATE));
            }
            JSONArray mArrayData = mOuterJsonObject.getJSONArray(GlobalKeys.RESPONSE_COMMENTS);
            for (int i = 0; i < mArrayData.length(); i++) {
                JSONObject mInnerJsonObject = mArrayData.getJSONObject(i);
                DiscussionBaseModel mBaseModel = new DiscussionBaseModel();
                if (mInnerJsonObject.has(GlobalKeys.COMMENT)) {
                    mBaseModel.setComment(mInnerJsonObject
                            .getString(GlobalKeys.COMMENT));
                }
                if (mInnerJsonObject.has(GlobalKeys.DATE)) {
                    mBaseModel.setComment_date(mInnerJsonObject
                            .getString(GlobalKeys.DATE));
                }
                if (mInnerJsonObject.has(GlobalKeys.USER)) {
                    JSONObject userJson = mInnerJsonObject.getJSONObject(GlobalKeys.USER);

                    if (userJson.has(GlobalKeys.PRAYER_REQUEST_EMAIL)) {
                        mBaseModel.setEmail(userJson
                                .getString(GlobalKeys.PRAYER_REQUEST_EMAIL));
                    }
                    if (userJson.has(GlobalKeys.FIRST_NAME)) {
                        mBaseModel.setFirst_name(userJson
                                .getString(GlobalKeys.FIRST_NAME));
                    }
                }
                mresponseBaseList.add(mBaseModel);
            }
//                }

            if (online) {
                saveResponseArray(responseBaseModel, mresponseBaseList);
            }
            mResponseListener.onSuccessOfResponse(responseBaseModel, mresponseBaseList);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        } else {
        /* Fail of API Response */
//            loadResponseArray();
//        }

    }


    /**
     * Check Trip API Listener
     */
    private WebAPIResponseListener downloadImageResponse(final String image_name) {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
                if (arguments != null && arguments.length > 0
                        && arguments[0] != null
                        && !arguments[0].toString().isEmpty()) {
                    Bitmap bitmap = (Bitmap) arguments[0];
                    AndroidAppUtils.showLog(TAG, "bitmap : " + bitmap);
                    AppUtils.savePassengerBitmapToSDCard(bitmap, image_name);

                    // if (position == mMassBaseList.size()) {
//                    mResponseListener.onSuccessOfResponse(mresponseBaseList);
                    // }
                }

            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
//                AndroidAppUtils.hideProgressDialog();
            }
        };
        return mListener;
    }


    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ResponseBaseModel responseBaseModel, ArrayList<DiscussionBaseModel> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(GlobalKeys.GET_DISCUSSION_KEY, json);
            String responsejson = gson.toJson(responseBaseModel);
            prefsEditor.putString(GlobalKeys.GET_DISCUSSION_RESPONSE_KEY, responsejson);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(GlobalKeys.GET_DISCUSSION_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<DiscussionBaseModel>>() {
            }.getType();

            String responsejson = appSharedPrefs.getString(GlobalKeys.GET_DISCUSSION_RESPONSE_KEY, "");
            java.lang.reflect.Type responsetype = new TypeToken<ResponseBaseModel>() {
            }.getType();
            mresponseBaseList = new ArrayList<DiscussionBaseModel>();
            mresponseBaseList = gson.fromJson(json, type);
            responseBaseModel = gson.fromJson(responsejson, responsetype);

            if (mresponseBaseList != null && mresponseBaseList.size() > 0) {
                mResponseListener.onSuccessOfResponse(responseBaseModel, mresponseBaseList);
            } else {
                mresponseBaseList = new ArrayList<DiscussionBaseModel>();
                mResponseListener.onFailOfResponse();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     * Read From File
     *
     * @param fileName
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }
}