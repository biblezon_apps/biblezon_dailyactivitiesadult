package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class GetDiscussionTitleAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetDiscussionTitleAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetDiscussionTitleAPIHandler(Activity mActivity,
                                        WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.DISCUSSION_TITLE).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                try {
                    parseAPIResponse(new JSONObject(ReadFromfile(
                            "discussion_title.txt", mActivity)));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getAuthToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.GET_DISCUSSION_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response) {
        /* Success of API Response */
        try {
            String Title = "";
            if (response.has("discussion"))
                Title = response.getString("discussion");

            Log.d(TAG, "Title :" + Title);
            if (mResponseListener != null)
                mResponseListener.onSuccessOfResponse(Title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Read From File
     *
     * @param fileName
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

}