package com.biblezon.DailyActivitiesAdult.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;

import java.util.ArrayList;

/**
 * Created by sonia on 13/8/15.
 */
public class ReflectionListAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<ResponseBaseModel> listItems;
    String TAG = ReflectionListAdapter.class.getSimpleName();
    Activity mActivity;

    public ReflectionListAdapter(Activity context, ArrayList<ResponseBaseModel> list) {
        mContext = context;
        this.mActivity = context;
        listItems = new ArrayList<ResponseBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public ResponseBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<ResponseBaseModel> updatedResponseModel, boolean single) {
        if (single) {
            if (updatedResponseModel != null && updatedResponseModel.size() > 0) {
                listItems = new ArrayList<ResponseBaseModel>();
                listItems.add(updatedResponseModel.get(0));
            }
        } else {
            listItems = new ArrayList<ResponseBaseModel>();
            listItems.addAll(updatedResponseModel);
        }
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext.getApplicationContext(),
                    R.layout.list_row_with_image, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (listItems != null) {
            final ResponseBaseModel responseBaseModel = listItems.get(position);
            holder.description.setText((Html.fromHtml(responseBaseModel.getDescription()).toString()));
            holder.title.setText(responseBaseModel.getTitle());
            String current_url = GlobalKeys.REFLECTION_KEY + "_" + responseBaseModel.getId();
            holder.image_view.setVisibility(View.GONE);
            if (responseBaseModel.getImage() != null
                    && !responseBaseModel.getImage().isEmpty()) {
//                new LoadImageFromCloud(mActivity, responseBaseModel.getImage(), holder.image_view);

            }
        }
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Shruti
     */
    class ViewHolder {
        TextView description, title;
        ImageView image_view;

        public ViewHolder(View view) {
            description = (TextView) view
                    .findViewById(R.id.description);
            title = (TextView) view
                    .findViewById(R.id.title);
            image_view = (ImageView) view
                    .findViewById(R.id.image_view);
            view.setTag(this);
        }
    }
}
