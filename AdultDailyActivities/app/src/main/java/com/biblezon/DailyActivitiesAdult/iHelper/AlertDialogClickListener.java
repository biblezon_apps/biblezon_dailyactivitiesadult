package com.biblezon.DailyActivitiesAdult.iHelper;

/**
 * This interface to manage Alert Dialog click
 *
 * @author Anshuman
 */
public interface AlertDialogClickListener {
    /* Listener Click */
    void onClickOfAlertDialogPositive();

}
