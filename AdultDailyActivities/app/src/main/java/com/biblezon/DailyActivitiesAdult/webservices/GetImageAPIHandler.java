package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;
import android.graphics.Bitmap;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;

import org.apache.commons.io.FilenameUtils;

/**
 * Get user image API Handler
 *
 * @author Shruti
 */
public class GetImageAPIHandler {
    int position;
    boolean isProfilePic = false;
    /**
     * Instance object of get image API
     */
    @SuppressWarnings("unused")
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetImageAPIHandler.class.getSimpleName();
    /**
     * Request Data
     */
    private String image_path;
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetImageAPIHandler(Activity mActivity, String image_path,
                              WebAPIResponseListener webAPIResponseListener,
                              boolean isProgreesShowing, int position, boolean isProfilePic) {

        this.mActivity = mActivity;
        this.image_path = image_path;
        this.mResponseListener = webAPIResponseListener;
        this.position = position;
        this.isProfilePic = isProfilePic;
        postAPICall();
    }

//    private static Bitmap codec(Bitmap src, Bitmap.CompressFormat format,
//                                int quality) {
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        src.compress(format, quality, os);
//
//        byte[] array = os.toByteArray();
//        return BitmapFactory.decodeByteArray(array, 0, array.length);
//    }
//
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
//    protected int sizeOf(Bitmap data) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
//            return data.getRowBytes() * data.getHeight();
//        } else {
//            return data.getByteCount();
//        }
//    }

    /**
     * Call API to Get User image
     */
    private void postAPICall() {
        @SuppressWarnings("deprecation")
        ImageRequest mImageRequest = new ImageRequest(image_path.trim(),
                new Listener<Bitmap>() {

                    @Override
                    public void onResponse(Bitmap bitmap) {
                        if (bitmap != null) {
                            AndroidAppUtils.showLog(TAG,
                                    "Success To get user image");

                            AndroidAppUtils.showLog(TAG, "This is not a profile pic request");
                            String baseName = FilenameUtils.getBaseName(image_path.trim());
                            if (mResponseListener != null)
                                mResponseListener.onSuccessOfResponse(bitmap, baseName);


                        } else {
                            AndroidAppUtils.showLog(TAG, "bitmap is null");
                            // mResponseListener.onFailOfResponse(mActivity
                            // .getResources().getString(R.string.RETRY));
                            if (mResponseListener != null)
                                mResponseListener.onFailOfResponse();
                        }
                    }
                }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showLog(TAG,
                        "get image onErrorResponse " + error);
                if (mResponseListener != null)
                    mResponseListener.onFailOfResponse();
            }
        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put(GlobalKeys.HEADER_CONTENT_TYPE,
//                        GlobalKeys.HEADER_APPLICATION_JOSN);
//                headers.put(GlobalKeys.HEADER_AUTHORIZATION_TOKEN,
//                        SessionManager.getInstance(mActivity).getAuthToken());
//                return headers;
//            }

        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToImageRequestQueue(
                mImageRequest, GetImageAPIHandler.class.getSimpleName() + position);
        // set request time-out
        mImageRequest.setRetryPolicy(new DefaultRetryPolicy(
                1000 * 60,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ReDriverApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.LOGIN_REQUEST_KEY);
    }

//    private void compressBitmap(Bitmap bitmap) {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
////        BitmapFactory.decodeResource(mActivity.getResources(), bitmap, options);
//        FileDescriptor fd = new FileInputStream();
//        BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
//        int imageHeight = options.outHeight;
//        int imageWidth = options.outWidth;
//        String imageType = options.outMimeType;
//    }
}