package com.biblezon.DailyActivitiesAdult.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;

import java.util.ArrayList;

/**
 * Created by sonia on 13/8/15.
 */
public class PrayerRequestListAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<String> listItems;
    String TAG = PrayerRequestListAdapter.class.getSimpleName();

    public PrayerRequestListAdapter(Context context, ArrayList<String> list) {
        mContext = context;
        listItems = new ArrayList<String>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public String getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<String> updatedResponseModel) {
        listItems = new ArrayList<String>();
        listItems.addAll(updatedResponseModel);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext.getApplicationContext(),
                    R.layout.prayer_list_row, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (listItems != null) {
            final String message = listItems.get(position);
            holder.title.setText(message);
        }
        return convertView;
    }

    /**
     * List view row object and its views
     *
     * @author Shruti
     */
    class ViewHolder {
        TextView title;

        public ViewHolder(View view) {
            title = (TextView) view
                    .findViewById(R.id.tv_title);
            view.setTag(this);
        }
    }
}
