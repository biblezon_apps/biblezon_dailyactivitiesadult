package com.biblezon.DailyActivitiesAdult.preference;

/**
 * Preference Helper Class
 *
 * @author Anshuman
 */
public interface PreferenceHelper {
    // Shared Preferences file name
    String PREFERENCE_NAME = "ADULT_DAILY_ACTIVITIES_PREFERENCE";
    // Shared Preferences mode
    int PRIVATE_MODE = 0;
    // User Id
    String KEY_USER_NAME = "USER_NAME";
    //User email id
    String KEY_USER_EMAIL = "USER_EMAIL";
    //Last Ratings Date
    String KEY_RATING_DATE = "rating_date";
    // authorization token Key
    String KEY_AUTHORIZATION_TOKEN = "AUTHORIZATION_TOKEN";
    // Login Shared Preferences Keys
    String IS_LOGIN = "IsLoggedIn";

}
