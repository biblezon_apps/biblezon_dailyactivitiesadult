package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Add message before ride API Handler
 *
 * @author Shruti
 */
public class PrayerRequestAPIHandler {
    /**
     * Instance object of Add message after ride API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = PrayerRequestAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private String message = "";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public PrayerRequestAPIHandler(Activity mActivity, String message,
                                   WebAPIResponseListener webAPIResponseListener) {
//        AndroidAppUtils.showProgressDialog(mActivity, "Loading...",
//                false);
        this.mActivity = mActivity;
        this.message = message;
        this.mResponseListener = webAPIResponseListener;
        postAPICallString();

    }

    /**
     * Making String object request
     */
    public void postAPICallString() {
        String URL = (GlobalKeys.BASE_URL + GlobalKeys.PRAYER_REQUEST).trim();
        AndroidAppUtils.showLog(TAG, "URL Post :" + URL);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL
                , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
//                AndroidAppUtils.hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                AndroidAppUtils.showErrorLog(
//                        TAG,
//                        WebserviceAPIErrorHandler.getInstance()
//                                .VolleyErrorHandlerReturningString(
//                                        error, mActivity));
                AndroidAppUtils.showLog(TAG, WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandlerReturningString(
                                error, mActivity));
                mResponseListener.onFailOfResponse();
//                AndroidAppUtils.hideProgressDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put(GlobalKeys.PRAYER_REQUEST_NAME, SessionManager.getInstance(mActivity).getUserName());
//                params.put(GlobalKeys.PRAYER_REQUEST_EMAIL, SessionManager.getInstance(mActivity).getUserEmail());
                params.put(GlobalKeys.PRAYER_REQUEST_MESSAGE, message);
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getAuthToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(strReq, GlobalKeys.PRAYER_REQUEST_KEY);
        // set request time-out
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalKeys.ONE_SECOND * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.CHANGE_KEY);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(String response) {
        // mResponseListener
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean status = WebserviceResponseHandler.getInstance()
                    .checkPrayerRequestResponseCode(jsonObject);
            if (status) {
            /* Response Success */
                mResponseListener.onSuccessOfResponse(response);

            } else {
            /* Response Status is null API Fail */
                mResponseListener.onFailOfResponse();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
