package com.biblezon.DailyActivitiesAdult.utils;
import 	java.util.Locale;
/**
 * Application Keys
 *
 * @author Anshuman
 */
public interface GlobalKeys {
    String IMAGE_BASE_URL = "http://biblezonadmin.com/biblezon/";//"http://biblezon.com/appapicms/app/webroot/uploads/images/";
    String BASE_URL = "http://biblezonadmin.com/biblezon/api/dailyactivity.php";//"http://biblezon.com/appapicms/webservices/";
    String ANNOUNCEMENT = "?action=getDailyReadings&category=ANNOUNCEMENT&lang="+Locale.getDefault().getDisplayLanguage();//"inbox/2/1";
    String READING = "?action=getDailyReadings&category=READING&lang="+Locale.getDefault().getDisplayLanguage();//"inbox/2/3";
    String REFLECTION = "?action=getDailyReadings&category=REFLECTION&lang="+Locale.getDefault().getDisplayLanguage(); //"inbox/2/2";
    String SAINT_OF_DAY = "?action=getDailyReadings&category=SAINT_OF_DAY&lang="+Locale.getDefault().getDisplayLanguage();//"inbox/2/4";
    String QUIZ_OF_DAY = "?action=quizofday&lang="+Locale.getDefault().getDisplayLanguage();//"quizofday/2";
    String PRAYER_REQUEST = "?action=PRAYER_REQUEST&lang="+Locale.getDefault().getDisplayLanguage();
    String ADULT_LOGIN = "?action=adult_login";
    String KIDS_LOGIN = "kids_login";
    String DASHBOARD_ADULT = "?action=DASHBOARD_ADULT&lang="+Locale.getDefault().getDisplayLanguage();
    String DISCUSSION_SUBMIT = "?action=discussion_submit_comment";
    String DISCUSSION = "?action=discussion_comments";

    String RESPONSE_DISCUSSION = "discussion";
    String RESPONSE_COMMENTS = "comments";
    String DISCUSSION_TITLE = "?action=discussion_title";

    String DASHBOARD_ANNOUNCEMENT = "Announcements";
    String DASHBOARD_READING = "Todays Readings";
    String DASHBOARD_REFLECTION = "Reflections";
    String DASHBOARD_SAINT_OF_DAY = "Saint of the day";
    String DASHBOARD_QUIZ_OF_DAY = "Quiz of the day";
    //    String DASHBOARD_PRAYER_REQUEST = "prayerrequest";
    String DASHBOARD_DISCUSSION = "Discussion";

    String SUBMIT_QUIZ = "submit_quiz_of_day";
    String AdultDailyActivities = "AdultDailyActivities";

    /* API Keys */
    String DASHBOARD_ADULT_KEY = "dashboard_adult_key";
    String ANNOUNCEMENT_KEY = "announcement";
    String READING_KEY = "reading";
    String REFLECTION_KEY = "reflection";
    String SAINT_OF_DAY_KEY = "saint_of_day";
    String QUIZ_OF_DAY_KEY = "quiz_of_day";
    String PRAYER_REQUEST_KEY = "prayer_request";
    String LOGIN_KEY = "login";
    String DISCUSSION_KEY = "discussion";
    String PRAYER_KEY = "prayer";
    String GET_PRYAER_API = "prayerrequest_list_messages/page:1";
    String GET_DISCUSSION_KEY = "get_discussion";
    String GET_DISCUSSION_RESPONSE_KEY = "get_discussion_response";
    String GET_IMAGE_KEY = "get_image";
    String SUBMIT_QUIZ_KEY = "submit_quiz";
    String PRAYERREQUEST_KEY = "prayer_key";

    String DASHBOARD_ANNOUNCEMENT_KEY = "dashboard_announcement";
    String DASHBOARD_READING_KEY = "dashboard_reading";
    String DASHBOARD_REFLECTION_KEY = "dashboard_reflection";
    String DASHBOARD_SAINT_OF_DAY_KEY = "dashboard_saint_of_day";
    String DASHBOARD_QUIZ_OF_DAY_KEY = "dashboard_quiz_of_day";
    String DASHBOARD_PRAYER_REQUEST_KEY = "dashboard_prayer_request";
    String DASHBOARD_DISCUSSION_KEY = "dashboard_discussion";

    String RESPONSE_CODE = "replyCode";
    String RESPONSE_MESSAGE = "replyMsg";
    String SUCCESS = "SUCCESS";
    String DATA = "data";
    String TOKEN = "token";
    String DATE = "date";
    String FIRST_NAME = "first_name";

    /* Response Activity Keys */
    String RESPONSE_ID = "id";
    String RESPONSE_TITLE = "title";
    String RESPONSE_DESCRIPTION = "description";
    String RESPONSE_IMAGE = "image";

    int ONE_SECOND = 1000;
    int API_TIME_OUT = 20;

    /**
     * version check keys
     */

//    String VERSION_CHECK_BASEURL = "http://biblezon.com/appapicms/webservices/checkversion/";
    String VERSION_CHECK_KEY = "version_check";
    String MSG_SUCCESS = "success";
    String LATEST_VERSION = "latestVersion";
    String UPDATED_APP_URL = "appURI";
    String APK_NAME = "adultdailyactivities";
    String AUTO_UPDATE_URL = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=com.biblezon.adultdailyactivities&deviceId=";
    /**
     * quiz of the day keys
     */
    String QUIZ_QUESTION = "question";
    String QUIZ_SCORE = "score";
    String QUIZ_SHOWSCORE = "showscore";
    String QUIZ_OPTION_COUNT = "answer_id";
    String QUIZ_OPTION_TITLE = "option";
    String QUIZ_OPTIONS = "options";
    String QUIZ_QUIZ_ID = "quiz_of_day_id";
    String QUIZ_OPTION_CORRECTION = "is_correct";
    String QUIZ_CORRECT_ANSWER = "correct_answer";

    /**
     * Prayer request keys
     */
    String PRAYER_REQUEST_NAME = "name";
    String PRAYER_REQUEST_EMAIL = "email";
    String PRAYER_REQUEST_MESSAGE = "message";
    String DISCUSSION_ID = "discussion_id";
    String COMMENT = "comment";
    String USER = "User";
    /**
     * Submit quiz keys
     */
    String QUIZ_ID = "quiz_of_day_id";
    String IS_CORRECT = "is_correct";
}
