package com.biblezon.DailyActivitiesAdult.control;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;


import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.webservices.GetImageAPIHandler;
import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.Random;

/**
 * Created by Anshuman on 6/18/2016.
 */
public class LoadImageFromCloud {
    /*Activity Instance*/
    private Activity mActivity;
    private String imageURl = "", baseName = "";
    private ImageView viewForImage;
    private String TAG = LoadImageFromCloud.class.getSimpleName();

    /**
     * Load image on Cloud
     */
    public LoadImageFromCloud(Activity mActivity, String imageURl, ImageView viewForImage) {
        this.mActivity = mActivity;
        this.imageURl = imageURl;
        this.viewForImage = viewForImage;
        AndroidAppUtils.showLog(TAG, "imageURl :" + imageURl);
        baseName = FilenameUtils.getBaseName(imageURl.trim());
        if (isFilePresent(mActivity, baseName))
            new ShowDeviceImageTask().execute();
        else
            startImageDownload();
    }

    /**
     * Start iamge Download
     */
    private void startImageDownload() {
        Random r = new Random();
        int randNumber = r.nextInt(1000 - 65) + 65;
        new GetImageAPIHandler(mActivity, imageURl, onImageDownloadListener(), false, randNumber, false);
    }

    /**
     * On Image download listerner
     *
     * @return
     */
    private WebAPIResponseListener onImageDownloadListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments.length > 0) {
                    Bitmap mdeviceImage = (Bitmap) arguments[0];
                    AndroidAppUtils.saveBitmapToSDCard(mdeviceImage, baseName);
                    if (viewForImage != null) {
                        viewForImage.setImageBitmap(mdeviceImage);
                        viewForImage.setVisibility(View.VISIBLE);
                    }
                }
                AndroidAppUtils.hideProgressDialog();
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                AndroidAppUtils.hideProgressDialog();
            }


        };
        return mListener;
    }

    /**
     * Show Already Saved Image Image Task
     */
    private class ShowDeviceImageTask extends AsyncTask<Void, Void, Void> {
        Bitmap deviceBitmap = null;

        public ShowDeviceImageTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            AndroidAppUtils.showProgressDialog(mActivity, mActivity.getResources().getString(R.string.loading), false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String root = Environment.getExternalStorageDirectory().toString();
                String photoPath = root + "/" + "WARRANTY_APP" + "/" + baseName + ".png";
                deviceBitmap = getImageFromStorage(mActivity, photoPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewForImage != null && deviceBitmap != null) {
                        viewForImage.setImageBitmap(deviceBitmap);
                        viewForImage.setVisibility(View.VISIBLE);
                    }
//                    else
//                        AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity, "Loading Image Fail, Please try again.", null);
                }
            });
            AndroidAppUtils.hideProgressDialog();
        }
    }

    /**
     * is File is exist in path or not
     *
     * @param mActivity
     * @param fileName
     * @return
     */
    public static boolean isFilePresent(Activity mActivity, String fileName) {
        String root = Environment.getExternalStorageDirectory().toString();
        String path = root + "/" + "WARRANTY_APP" + "/" + fileName + ".png";
        File file = new File(path);
        return file.exists();
    }

    /**
     * Load Image from bitmap
     *
     * @param mActivity
     * @param filePath
     * @return
     */
    public static Bitmap getImageFromStorage(Activity mActivity, String filePath) {
        Bitmap bmp = BitmapFactory.decodeFile(filePath);
        return bmp;
    }

}
