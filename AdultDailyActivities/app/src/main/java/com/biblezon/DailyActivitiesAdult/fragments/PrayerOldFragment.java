//package com.biblezon.DailyActivitiesAdult.fragments;
//
//import android.app.Activity;
//import android.content.SharedPreferences;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.biblezon.DailyActivitiesAdult.MainActivity;
//import com.biblezon.DailyActivitiesAdult.R;
//import com.biblezon.DailyActivitiesAdult.adapter.PrayerAdapter;
//import com.biblezon.DailyActivitiesAdult.control.HeaderViewManager;
//import com.biblezon.DailyActivitiesAdult.iHelper.AlertDialogClickListener;
//import com.biblezon.DailyActivitiesAdult.iHelper.HeaderViewClickListener;
//import com.biblezon.DailyActivitiesAdult.model.DiscussionBaseModel;
//import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
//import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
//import com.biblezon.DailyActivitiesAdult.utils.AppDialogUtils;
//import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;
//import com.biblezon.DailyActivitiesAdult.webservices.GetPrayerBaseAPIHandler;
//import com.biblezon.DailyActivitiesAdult.webservices.PrayerRequestAPIHandler;
//import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//
//import java.util.ArrayList;
//
//
///**
// * Slider fragment Help Screen
// *
// * @author Anshuman
// */
//public class PrayerOldFragment extends Fragment {
//
//    private String TAG = DiscussionFragment.class.getSimpleName();
//    private Activity mActivity;
//    /**
//     * Screen base view
//     */
//    private View mView;
//    ListView request_listview;
//    private PrayerAdapter mListAdapter;
//    //    private ArrayList<String> messageList;
//    private ArrayList<DiscussionBaseModel> discussionList;
//    TextView send_button, date;
//    EditText message_edit;
//    String new_message;
//    RelativeLayout message_box;
//    ProgressBar progressBar1;
//    boolean canPostPrayer;
//
//    /*
//    * (non-Javadoc)
//    *
//    * @see
//    * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
//    * android.view.ViewGroup, android.os.Bundle)
//    */
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        mView = inflater.inflate(R.layout.prayer, container, false);
//        initViews();
//        assignClicks();
//        manageHeaderOfScreen();
//        MainActivity.getInstance().showProgressBar(true);
//        new GetPrayerBaseAPIHandler(mActivity, GetApiResponseListener());
////        loadResponseArray();
//        return mView;
//    }
//
//
//    /**
//     * ManageHeader of the screen
//     */
//    private void manageHeaderOfScreen() {
//        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
//                null);
//        HeaderViewManager.getInstance().setHeading(true,
//                mActivity.getResources().getString(R.string.prayer_request));
//    }
//
//    private void assignClicks() {
//        send_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                /**
//                 * hide keyboard
//                 */
//                AndroidAppUtils.keyboardDown(mActivity);
//
//                /**
//                 * check if message is null or empty or contains only space
//                 */
//                String message = message_edit.getText().toString();
//                if (message != null) {
//                    message = message.trim().replace(" ", "");
//                    if (message.isEmpty())
//                        new_message = "";
//                    else
//                        new_message = message_edit.getText().toString();
//                }
//                if (new_message != null && !new_message.isEmpty()) {
//                    progressBar1.setVisibility(View.VISIBLE);
//                    send_button.setText("");
//                    new PrayerRequestAPIHandler(mActivity, new_message, ApiResponseListener());
//                } else {
//                    AppDialogUtils.showAlertDialog(mActivity, "Message can not be empty", "Ok", new AlertDialogClickListener() {
//                        @Override
//                        public void onClickOfAlertDialogPositive() {
//                            message_edit.setText("");
//                        }
//                    });
//                }
//            }
//        });
//    }
//
//    /**
//     * initializing view fields
//     */
//    private void initViews() {
//        mActivity = getActivity();
//        request_listview = (ListView) mView.findViewById(R.id.request_listview);
//        message_box = (RelativeLayout) mView.findViewById(R.id.message_box);
//        message_box.setVisibility(View.GONE);
//        send_button = (TextView) mView.findViewById(R.id.send_button);
//        message_edit = (EditText) mView.findViewById(R.id.message_edit);
//        progressBar1 = (ProgressBar) mView.findViewById(R.id.progressBar1);
//        progressBar1.setVisibility(View.GONE);
//        request_listview.setCacheColorHint(Color.TRANSPARENT);
//        request_listview.requestFocus(0);
//        request_listview.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//        request_listview.setStackFromBottom(true);
////        messageList = new ArrayList<>();
//        discussionList = new ArrayList<>();
//        mListAdapter = new PrayerAdapter(mActivity,
//                discussionList);
//        request_listview.setAdapter(mListAdapter);
//        String user_email = SessionManager.getInstance(mActivity).getUserEmail();
//        if (user_email != null && !user_email.isEmpty()) {
//            if (user_email.contains("biblezon.com")) {
//                canPostPrayer = true;
//            } else {
//                canPostPrayer = false;
//            }
//        } else {
//            canPostPrayer = false;
//        }
//    }
//
//    private void addDataIntoList() {
//        // TODO Auto-generated method stub
//        mListAdapter.updateListData(discussionList);
//        mListAdapter.notifyDataSetChanged();
//    }
//
//    private WebAPIResponseListener ApiResponseListener() {
//        WebAPIResponseListener mListener = new WebAPIResponseListener() {
//
//            @Override
//            public void onSuccessOfResponse(Object... arguments) {
////                messageList.add(new_message);
////                addDataIntoList();
////                saveResponseArray(messageList);
////                message_edit.setText("");
////                progressBar1.setVisibility(View.GONE);
////                send_button.setText("Send");
//                new GetPrayerBaseAPIHandler(mActivity, GetApiResponseListener());
//            }
//
//            @Override
//            public void onFailOfResponse(Object... arguments) {
////                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity, "Network Error",
////                        "Please check your internet connection", R.color.black, R.color.black, null);
//                progressBar1.setVisibility(View.GONE);
//                send_button.setText("Send");
//            }
//
//        };
//        return mListener;
//    }
//
//    private WebAPIResponseListener GetApiResponseListener() {
//        WebAPIResponseListener mListener = new WebAPIResponseListener() {
//
//            @Override
//            public void onSuccessOfResponse(Object... arguments) {
//                MainActivity.getInstance().showProgressBar(false);
//                message_edit.setText("");
//                progressBar1.setVisibility(View.GONE);
//                send_button.setText("Send");
//                if (arguments != null && arguments.length > 0) {
//                    if (canPostPrayer) {
//                        message_box.setVisibility(View.VISIBLE);
//                    }
//                    if (arguments[0] != null) {
//                        discussionList = new ArrayList<>();
//                        discussionList = (ArrayList<DiscussionBaseModel>) arguments[0];
//                        addDataIntoList();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailOfResponse(Object... arguments) {
//                MainActivity.getInstance().showProgressBar(false);
////                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity, "Network Error",
////                        "Please check your internet connection", R.color.black, R.color.black, null);
//            }
//
//        };
//        return mListener;
//    }
//
//
//    /**
//     * On Click of Header left & Right View
//     *
//     * @return
//     */
//    private HeaderViewClickListener ClickOfHeaderLeftRightView() {
//        HeaderViewClickListener mListener = new HeaderViewClickListener() {
//
//            @Override
//            public void onClickOfHeaderRightView() {
//            }
//
//            @Override
//            public void onClickOfHeaderLeftView() {
//                mActivity.onBackPressed();
//            }
//        };
//        return mListener;
//    }
//
//    /**
//     * Save Notes Data into shared Preferences
//     *
//     * @param mbitArray
//     * @return
//     */
//    private boolean saveResponseArray(ArrayList<String> mbitArray) {
//        try {
//            SharedPreferences appSharedPrefs = PreferenceManager
//                    .getDefaultSharedPreferences(mActivity);
//            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
//            Gson gson = new Gson();
//            String json = gson.toJson(mbitArray);
//            prefsEditor.putString(GlobalKeys.PRAYER_KEY, json);
//            return prefsEditor.commit();
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    /**
//     * Restore List data from shared Preferences
//     */
//    @SuppressWarnings("unused")
//    private void loadResponseArray() {
//        AndroidAppUtils.showLog(TAG, "loadResponseArray");
//        try {
//            SharedPreferences appSharedPrefs = PreferenceManager
//                    .getDefaultSharedPreferences(mActivity);
//            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
//            Gson gson = new Gson();
//            String json = appSharedPrefs.getString(GlobalKeys.PRAYER_KEY, "");
//            java.lang.reflect.Type type = new TypeToken<ArrayList<DiscussionBaseModel>>() {
//            }.getType();
//            discussionList = new ArrayList<DiscussionBaseModel>();
//            discussionList = gson.fromJson(json, type);
//
//            if (discussionList != null && discussionList.size() > 0) {
//                addDataIntoList();
//            } else {
//                discussionList = new ArrayList<DiscussionBaseModel>();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
