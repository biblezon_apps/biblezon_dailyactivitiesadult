package com.biblezon.DailyActivitiesAdult.control;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.biblezon.DailyActivitiesAdult.MainActivity;
import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.fragments.AnnounceFragment;
import com.biblezon.DailyActivitiesAdult.fragments.DashboardFragment;
import com.biblezon.DailyActivitiesAdult.fragments.DiscussionFragment;
import com.biblezon.DailyActivitiesAdult.fragments.PrayerFragment;
import com.biblezon.DailyActivitiesAdult.fragments.QuizOfDayFragment;
import com.biblezon.DailyActivitiesAdult.fragments.ReadingFragment;
import com.biblezon.DailyActivitiesAdult.fragments.ReflectionFragment;
import com.biblezon.DailyActivitiesAdult.fragments.SaintOfDayFragment;


/**
 * Manage Left Side Sliding Menu
 *
 * @author Anshuman
 */
public class LeftSlidingMenuControl implements OnClickListener {
    private static Activity mActivity;
    /**
     * Manage Menu Slider layout view object
     */

    private LinearLayout selectedViews[] = new LinearLayout[7];
    private LinearLayout unselectedViews[] = new LinearLayout[7];
    /**
     * Currently Selected TAG
     */
    private int currentSelectedTAG = 0;
    public static LeftSlidingMenuControl mLeftSlidingMenuControl;

    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = LeftSlidingMenuControl.class.getSimpleName();

    /**
     * Instance of this class
     *
     * @return
     */
    public static LeftSlidingMenuControl getInstance() {

        return mLeftSlidingMenuControl;
    }

    /**
     * Left Side Sliding Menu Control constructor
     */
    public LeftSlidingMenuControl(Activity mAct) {
        mActivity = mAct;
        mLeftSlidingMenuControl = this;
        initSlidingView();
        assignClicks();
        assignTags();
    }

    /**
     * Manage Sliding View object
     */
    private void initSlidingView() {

        /*
        initialize selected linear layouts
         */
        selectedViews[0] = (LinearLayout) mActivity.findViewById(R.id.announceViewSelected);
        selectedViews[1] = (LinearLayout) mActivity.findViewById(R.id.readingViewSelected);
        selectedViews[2] = (LinearLayout) mActivity.findViewById(R.id.reflectionViewSelected);
        selectedViews[3] = (LinearLayout) mActivity.findViewById(R.id.saintViewSelected);
        selectedViews[4] = (LinearLayout) mActivity.findViewById(R.id.quizViewSelected);
        selectedViews[5] = (LinearLayout) mActivity.findViewById(R.id.prayerRequestViewSelected);
        selectedViews[6] = (LinearLayout) mActivity.findViewById(R.id.discussionViewSelected);
         /*
        initialize unselected linear layouts
         */
        unselectedViews[0] = (LinearLayout) mActivity.findViewById(R.id.announceView);
        unselectedViews[1] = (LinearLayout) mActivity.findViewById(R.id.readingView);
        unselectedViews[2] = (LinearLayout) mActivity.findViewById(R.id.reflectionView);
        unselectedViews[3] = (LinearLayout) mActivity.findViewById(R.id.saintView);
        unselectedViews[4] = (LinearLayout) mActivity.findViewById(R.id.quizView);
        unselectedViews[5] = (LinearLayout) mActivity.findViewById(R.id.prayerRequestView);
        unselectedViews[6] = (LinearLayout) mActivity.findViewById(R.id.discussionView);
    }

    /**
     * Assign click to View objects
     */
    private void assignClicks() {
        for (int i = 0; i < selectedViews.length; i++) {
            unselectedViews[i].setOnClickListener(this);
            unselectedViews[i].setVisibility(View.VISIBLE);
            selectedViews[i].setVisibility(View.GONE);
        }
        selectedViews[0].setOnClickListener(this);
    }

    /**
     * Assign TAG on GUI
     */
    private void assignTags() {
        unselectedViews[0].performClick();
        currentSelectedTAG = selectedViews[0].getId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.announceView:
                callNextFragment(0);
                break;
            case R.id.announceViewSelected:
                if (currentSelectedTAG == -1) {
                    mActivity.onBackPressed();
                }
                break;
            case R.id.readingView:
                callNextFragment(1);
                break;
            case R.id.reflectionView:
                callNextFragment(2);
                break;
            case R.id.saintView:
                callNextFragment(3);
                break;
            case R.id.quizView:
                callNextFragment(4);
                break;
            case R.id.prayerRequestView:
                callNextFragment(5);
                break;
            case R.id.discussionView:
                callNextFragment(6);
                break;
            default:

                break;
        }
    }

    public void callNextFragment(int position) {
        if (position >= 0) {
            if (currentSelectedTAG != selectedViews[position].getId()) {
                manageViewVisibility(position);
                currentSelectedTAG = selectedViews[position].getId();
                MainActivity.getInstance().removeAllOldFragment();
                MainActivity.getInstance().showProgressBar(false);
                switch (position) {
                    case 0:
                        MainActivity.getInstance().pushFragment(new DashboardFragment());
                        break;
                    case 1:
                        MainActivity.getInstance().pushFragment(new ReadingFragment());
                        break;
                    case 2:
                        MainActivity.getInstance().pushFragment(new ReflectionFragment());
                        break;
                    case 3:
                        MainActivity.getInstance().pushFragment(new SaintOfDayFragment());
                        break;
                    case 4:
                        MainActivity.getInstance().pushFragment(new QuizOfDayFragment());
                        break;
                    case 5:
                        MainActivity.getInstance().pushFragment(new PrayerFragment());
                        break;
                    case 6:
                        MainActivity.getInstance().pushFragment(new DiscussionFragment());
                        break;
                }
            }
        } else {
            currentSelectedTAG = -1;
            MainActivity.getInstance().removeAllOldFragment();
            MainActivity.getInstance().showProgressBar(false);
            MainActivity.getInstance().pushFragment(new AnnounceFragment());
        }

    }

    public void callDashboard() {
        assignTags();
    }

    public void manageViewVisibility(int current) {
        for (int index = 0; index < selectedViews.length; index++) {
            if (index == current) {
                selectedViews[index].setVisibility(View.VISIBLE);
                unselectedViews[index].setVisibility(View.GONE);
            } else {
                selectedViews[index].setVisibility(View.GONE);
                unselectedViews[index].setVisibility(View.VISIBLE);
            }
        }
    }
}
