package com.biblezon.DailyActivitiesAdult;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.preference.SessionManager;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.AppDialogUtils;
import com.biblezon.DailyActivitiesAdult.webservices.LoginAPIHandler;
import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Shruti on 1/6/2016.
 */
public class LoginActivity extends Activity {
    Activity mActivity;
    TextView submit_button;
    EditText email_et, name_et;
    String email, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        initViews();
        assignClicks();
    }

    private void assignClicks() {
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationBeforeLogin())
                    new LoginAPIHandler(mActivity, name, email, ApiResponseListener());
            }
        });
    }

    private WebAPIResponseListener ApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null && !arguments[0].toString().isEmpty()) {
                    SessionManager.getInstance(mActivity).createLoginSession(name, email, arguments[0].toString(), getCurrentDate());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        mActivity.getResources().getString(R.string.try_again),
                        "", R.color.black, R.color.orange, null);
            }
        };
        return mListener;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
//        2015 - 12 - 30
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private void initViews() {
        mActivity = this;
        submit_button = (TextView) findViewById(R.id.submit_button);
        email_et = (EditText) findViewById(R.id.email_et);
        name_et = (EditText) findViewById(R.id.name_et);
    }

    /**
     * Check the validations on the view fields before performing login task
     */
    protected boolean checkValidationBeforeLogin() {
        if (email_et == null || name_et == null) {
            AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                    mActivity.getResources().getString(R.string.try_again),
                    "", R.color.black, R.color.orange, null);
            return false;
        } else {
            name = name_et.getText().toString();
            email = email_et.getText().toString();
            if (name == null || name.isEmpty()) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter your name.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else if (email == null || email.isEmpty()) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter your email id.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else if (!AndroidAppUtils.isEmailIDValidate(email)) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter valid email id.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else {
                return true;
            }
        }
    }

}
