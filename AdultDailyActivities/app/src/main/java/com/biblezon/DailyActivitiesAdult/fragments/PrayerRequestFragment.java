package com.biblezon.DailyActivitiesAdult.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.adapter.PrayerRequestListAdapter;
import com.biblezon.DailyActivitiesAdult.control.HeaderViewManager;
import com.biblezon.DailyActivitiesAdult.iHelper.AlertDialogClickListener;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.AppDialogUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;
import com.biblezon.DailyActivitiesAdult.webservices.PrayerRequestAPIHandler;
import com.biblezon.DailyActivitiesAdult.webservices.WebAPIResponseListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class PrayerRequestFragment extends Fragment {

    private String TAG = PrayerRequestFragment.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    ListView request_listview;
    private PrayerRequestListAdapter mListAdapter;
    private ArrayList<String> messageList;
    TextView send_button;
    EditText message_edit;
    String new_message;
    ProgressBar progressBar1;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.prayer_request, container, false);
        initViews();
        manageHeaderOfScreen();
        assignClicks();
        loadResponseArray();
        return mView;
    }

    private void assignClicks() {
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * hide keyboard
                 */
                AndroidAppUtils.keyboardDown(mActivity);

                /**
                 * check if message is null or empty or contains only space
                 */
                String message = message_edit.getText().toString();
                if (message != null) {
                    message = message.trim().replace(" ", "");
                    if (message.isEmpty())
                        new_message = "";
                    else
                        new_message = message_edit.getText().toString();
                }
                if (new_message != null && !new_message.isEmpty()) {
                    progressBar1.setVisibility(View.VISIBLE);
                    send_button.setText("");
                    new PrayerRequestAPIHandler(mActivity, new_message, ApiResponseListener());
                } else {
                    AppDialogUtils.showAlertDialog(mActivity, "Message can not be empty", "Ok", new AlertDialogClickListener() {
                        @Override
                        public void onClickOfAlertDialogPositive() {
                            message_edit.setText("");
                        }
                    });
                }
            }
        });
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        request_listview = (ListView) mView.findViewById(R.id.request_listview);
        send_button = (TextView) mView.findViewById(R.id.send_button);
        message_edit = (EditText) mView.findViewById(R.id.message_edit);
        progressBar1 = (ProgressBar) mView.findViewById(R.id.progressBar1);
        progressBar1.setVisibility(View.GONE);
        request_listview.setCacheColorHint(Color.TRANSPARENT);
        request_listview.requestFocus(0);
        request_listview.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        request_listview.setStackFromBottom(true);
        messageList = new ArrayList<>();
        mListAdapter = new PrayerRequestListAdapter(mActivity,
                messageList);
        request_listview.setAdapter(mListAdapter);
    }


    private void addDataIntoList() {
        // TODO Auto-generated method stub
        mListAdapter.updateListData(messageList);
        mListAdapter.notifyDataSetChanged();
    }

    private WebAPIResponseListener ApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                messageList.add(new_message);
                addDataIntoList();
                saveResponseArray(messageList);
                message_edit.setText("");
                progressBar1.setVisibility(View.GONE);
                send_button.setText("Send");
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity, "Network Error",
                        "Please check your internet connection", R.color.black, R.color.black, null);
                progressBar1.setVisibility(View.GONE);
                send_button.setText("Send");
            }
        };
        return mListener;
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                null);
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.prayer_request));
    }


    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ArrayList<String> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(GlobalKeys.PRAYER_REQUEST_KEY, json);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(GlobalKeys.PRAYER_REQUEST_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            messageList = new ArrayList<String>();
            messageList = gson.fromJson(json, type);

            if (messageList != null && messageList.size() > 0) {
                addDataIntoList();
            } else {
                messageList = new ArrayList<String>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
