package com.biblezon.DailyActivitiesAdult.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.DailyActivitiesAdult.R;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;

/**
 * Created by sonia on 13/8/15.
 */
public class BasicListAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<ResponseBaseModel> listItems;
    String TAG = BasicListAdapter.class.getSimpleName();

    public BasicListAdapter(Context context, ArrayList<ResponseBaseModel> list) {
        mContext = context;
        listItems = new ArrayList<ResponseBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public ResponseBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<ResponseBaseModel> updatedResponseModel) {
        listItems = new ArrayList<ResponseBaseModel>();
        listItems.addAll(updatedResponseModel);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_row, null);
        } else {
            view = convertView;
        }
        ResponseBaseModel responseBaseModel = listItems.get(position);
        if (responseBaseModel != null) {
            TextView titleView = (TextView) view.findViewById(R.id.tv_title);
            // TextView serialview = (TextView) view
            // .findViewById(R.id.serial_number);
            titleView.setText((Html.fromHtml(responseBaseModel.getDescription()).toString()));
            // serialview.setText(responseBaseModel.getActivityID() + ".)");
        }
        return view;
    }
}
