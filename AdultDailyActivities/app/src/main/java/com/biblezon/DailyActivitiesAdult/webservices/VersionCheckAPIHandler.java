package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;

import org.json.JSONObject;

/**
 * check version of application
 *
 * @author Shruti
 */
public class VersionCheckAPIHandler {
    private Activity mActivity;
    private Context context;
    /**
     * Debug TAG
     */
    private String TAG = VersionCheckAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;

    /**
     * @param mActivity
     * @param webAPIResponseListener //     * @param MassUrl
     */
    public VersionCheckAPIHandler(Activity mActivity,
                                  WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.context = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();
    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        String AutoUpdateURL = GlobalKeys.AUTO_UPDATE_URL + Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d(TAG, "AutoUpdateURL :" + AutoUpdateURL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                AutoUpdateURL, "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                if (mResponseListener != null)
                    mResponseListener.onSuccessOfResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                mResponseListener.onSuccessOfResponse();
            }
        }) {

        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.VERSION_CHECK_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
}
