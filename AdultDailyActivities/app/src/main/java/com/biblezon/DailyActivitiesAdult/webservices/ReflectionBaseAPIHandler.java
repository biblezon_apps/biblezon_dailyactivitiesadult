package com.biblezon.DailyActivitiesAdult.webservices;

import android.app.Activity;
import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.DailyActivitiesAdult.application.AppApplicationController;
import com.biblezon.DailyActivitiesAdult.model.ResponseBaseModel;
import com.biblezon.DailyActivitiesAdult.utils.AndroidAppUtils;
import com.biblezon.DailyActivitiesAdult.utils.GlobalKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class ReflectionBaseAPIHandler {

    private String chosenDate;

    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = ReflectionBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<ResponseBaseModel> mresponseBaseList = new ArrayList<ResponseBaseModel>();

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public ReflectionBaseAPIHandler(Activity mActivity,
                                    WebAPIResponseListener webAPIResponseListener, String chosenDate) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        this.chosenDate = chosenDate;
        postAPICall();

    }

    /**
     * Making json object request
     *
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.REFLECTION +"&chosendate="+this.chosenDate).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response, true);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                try {
                    parseAPIResponse(new JSONObject(ReadFromfile(
                            "reflection.txt", mActivity)), false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

//
            }
        }) {
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                ReflectionBaseAPIHandler.class.getSimpleName());
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response, boolean online) {
        if (response != null && WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
        /* Success of API Response */
            try {
                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
                for (int i = 0; i < mArrayData.length(); i++) {
                    JSONObject mOuterJsonObject = mArrayData.getJSONObject(i);
                    ResponseBaseModel mBaseModel = new ResponseBaseModel();
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_ID)) {
                        mBaseModel.setId(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_ID));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_TITLE)) {
                        mBaseModel.setTitle(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_TITLE));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_DESCRIPTION)) {
                        mBaseModel.setDescription(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_DESCRIPTION));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.DATE)) {
                        mBaseModel.setDate(mOuterJsonObject
                                .getString(GlobalKeys.DATE));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_IMAGE)) {
                        mBaseModel.setImage(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_IMAGE));

                    }
                    mresponseBaseList.add(mBaseModel);
                }

                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Read From File
     *
     * @param fileName
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public String ReadFromfile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName, Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }


}