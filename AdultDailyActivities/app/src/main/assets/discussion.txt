{
    "discussion": {
        "id": "21",
        "title": "Today's discussion (Heading)",
        "date": "2016-01-13",
        "description": "<p>Biblezone.</p>\r\n",
        "video": "http://goo.com",
        "image": "http://biblezon.com/appapicms/uploads/inboxs/1452698246Awards1.jpg",
        "comments": [
            {
                "id": "58",
                "user_id": "1143",
                "discussion_id": "21",
                "comment": "Alicia,  I would tell him he is hurting you but that you forgive him and are praying for him to become stronger in  virtue.",
                "date": "2016-06-23 09:03:44",
                "User": {
                    "id": "1143",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Bill Donovan",
                    "last_name": "",
                    "email": "wmadonovan@msn.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-06-23 02:48:35",
                    "updated": "2016-06-23 02:48:35"
                }
            },
            {
                "id": "57",
                "user_id": "1127",
                "discussion_id": "21",
                "comment": "Just figuring this \nthing out !!  Patience !!!",
                "date": "2016-06-20 20:09:58",
                "User": {
                    "id": "1127",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Bill Brady",
                    "last_name": "",
                    "email": "williambrady62@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-06-20 14:04:16",
                    "updated": "2016-06-20 14:04:16"
                }
            },
            {
                "id": "56",
                "user_id": "1122",
                "discussion_id": "21",
                "comment": "Just received my biblezon this is an awesome tool to continue my growth as a catholic",
                "date": "2016-06-18 21:53:47",
                "User": {
                    "id": "1122",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Brett",
                    "last_name": "",
                    "email": "brose9@aol.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-06-18 15:44:09",
                    "updated": "2016-06-18 15:44:09"
                }
            },
            {
                "id": "55",
                "user_id": "1014",
                "discussion_id": "21",
                "comment": "I join with you in prayer.",
                "date": "2016-06-18 13:35:40",
                "User": {
                    "id": "1014",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Bonnie Goard",
                    "last_name": "",
                    "email": "bonnje@embarqmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-10 06:59:22",
                    "updated": "2016-05-10 06:59:22"
                }
            },
            {
                "id": "54",
                "user_id": "1015",
                "discussion_id": "21",
                "comment": "I am trying to stay away from the internet because\nI lose track of time. I am having trouble even with only using my biblezon bible etc. please pray for me????",
                "date": "2016-06-17 18:20:30",
                "User": {
                    "id": "1015",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Judith",
                    "last_name": "",
                    "email": "judithh28603@yahoo.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-10 12:10:31",
                    "updated": "2016-05-10 12:10:31"
                }
            },
            {
                "id": "53",
                "user_id": "1094",
                "discussion_id": "21",
                "comment": "yes",
                "date": "2016-06-15 14:08:30",
                "User": {
                    "id": "1094",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Linda",
                    "last_name": "",
                    "email": "lynch.chris@sbcglobal.net",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-06-12 17:44:14",
                    "updated": "2016-06-12 17:44:14"
                }
            },
            {
                "id": "52",
                "user_id": "1063",
                "discussion_id": "21",
                "comment": "forgiveness is a must do, it saves your heart from hurt and takes away anger therefore, it saves you from temptation!",
                "date": "2016-06-11 01:43:45",
                "User": {
                    "id": "1063",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Alicia Dillon",
                    "last_name": "",
                    "email": "65@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-31 18:12:43",
                    "updated": "2016-05-31 18:12:43"
                }
            },
            {
                "id": "51",
                "user_id": "1086",
                "discussion_id": "21",
                "comment": "I am in the process of learning to forgive myself as well as others.",
                "date": "2016-06-10 18:09:06",
                "User": {
                    "id": "1086",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Georgiana",
                    "last_name": "",
                    "email": "charlyjones65@yahoo.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-06-10 11:57:06",
                    "updated": "2016-06-10 11:57:06"
                }
            },
            {
                "id": "50",
                "user_id": "1063",
                "discussion_id": "21",
                "comment": "Thank you very much for your help! I will not give up, may God bless you!",
                "date": "2016-06-08 18:14:17",
                "User": {
                    "id": "1063",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Alicia Dillon",
                    "last_name": "",
                    "email": "65@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-31 18:12:43",
                    "updated": "2016-05-31 18:12:43"
                }
            },
            {
                "id": "49",
                "user_id": "1020",
                "discussion_id": "21",
                "comment": "Forgiveness is often not deserved but is cleansing to the one doing the forgiving.",
                "date": "2016-06-08 00:22:57",
                "User": {
                    "id": "1020",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Patricia McKenna",
                    "last_name": "",
                    "email": "pmckenna101@yahoo.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-12 20:09:38",
                    "updated": "2016-05-12 20:09:38"
                }
            },
            {
                "id": "48",
                "user_id": "1014",
                "discussion_id": "21",
                "comment": "Forgiveness is sometimes very hard to do. It is essential that we forgive others as we ask God to forgive us. ",
                "date": "2016-06-07 11:56:51",
                "User": {
                    "id": "1014",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Bonnie Goard",
                    "last_name": "",
                    "email": "bonnje@embarqmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-10 06:59:22",
                    "updated": "2016-05-10 06:59:22"
                }
            },
            {
                "id": "47",
                "user_id": "1041",
                "discussion_id": "21",
                "comment": "Hello brothers and sisters in Christ. \n\nPlease share your thoughts on our discussion topic of the day...\n\n      \"FORGIVENESS\"",
                "date": "2016-06-07 11:28:34",
                "User": {
                    "id": "1041",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Biblezon",
                    "last_name": "",
                    "email": "info@biblezon.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-19 08:20:56",
                    "updated": "2016-05-19 08:20:56"
                }
            },
            {
                "id": "46",
                "user_id": "1023",
                "discussion_id": "21",
                "comment": "Please keep praying. I know it’s hard, I have found immense relief with the novena of confidence, Our Lady Undoer of Knots and novena to St Philomena. God is there holding you and helping you. ",
                "date": "2016-06-06 13:04:55",
                "User": {
                    "id": "1023",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Trina",
                    "last_name": "",
                    "email": "ltmuhs@utma.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-13 17:15:57",
                    "updated": "2016-05-13 17:15:57"
                }
            },
            {
                "id": "45",
                "user_id": "1063",
                "discussion_id": "21",
                "comment": "Please is there anyone who can give me some advice? My son is still with me with no intentions of moving out or looking for work! I just don't know what to do!",
                "date": "2016-06-05 23:30:57",
                "User": {
                    "id": "1063",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Alicia Dillon",
                    "last_name": "",
                    "email": "65@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-31 18:12:43",
                    "updated": "2016-05-31 18:12:43"
                }
            },
            {
                "id": "44",
                "user_id": "1063",
                "discussion_id": "21",
                "comment": "PS. I 'm hoping it would  it would change him but no difference. He also disrespects me. and don't give me no help!! I pray always for him need help!",
                "date": "2016-06-03 01:02:27",
                "User": {
                    "id": "1063",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Alicia Dillon",
                    "last_name": "",
                    "email": "65@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-31 18:12:43",
                    "updated": "2016-05-31 18:12:43"
                }
            },
            {
                "id": "43",
                "user_id": "1063",
                "discussion_id": "21",
                "comment": "I have a 21 yr old son living home with me. I'm disabled .receiving very low income.My problem is my son is not working he keeps quitting jobs ,I'm feeding him & he lies around all day playing games or hanging out with friends he's not looking for work.The stress is making me sick, I told  him to move out .because I can' afford to feed him anymore but now I'm hurting & feeling guilty any advice anyone?",
                "date": "2016-06-03 00:31:57",
                "User": {
                    "id": "1063",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Alicia Dillon",
                    "last_name": "",
                    "email": "65@gmail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-31 18:12:43",
                    "updated": "2016-05-31 18:12:43"
                }
            },
            {
                "id": "42",
                "user_id": "1023",
                "discussion_id": "21",
                "comment": "I have started sewing my daughter's dresses so they are longer.  I hate most styles in the store.",
                "date": "2016-05-30 23:22:44",
                "User": {
                    "id": "1023",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Trina",
                    "last_name": "",
                    "email": "ltmuhs@utma.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-13 17:15:57",
                    "updated": "2016-05-13 17:15:57"
                }
            },
            {
                "id": "41",
                "user_id": "1020",
                "discussion_id": "21",
                "comment": "I've been out to buy dresses with my daughter. It's hard to find anything else in the stores.",
                "date": "2016-05-30 03:52:56",
                "User": {
                    "id": "1020",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Patricia McKenna",
                    "last_name": "",
                    "email": "pmckenna101@yahoo.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-12 20:09:38",
                    "updated": "2016-05-12 20:09:38"
                }
            },
            {
                "id": "40",
                "user_id": "1056",
                "discussion_id": "21",
                "comment": "Why would they dress that way in our Lords house?",
                "date": "2016-05-29 01:43:49",
                "User": {
                    "id": "1056",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Rogers29761",
                    "last_name": "",
                    "email": "Aliciadillon65@gail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-28 16:49:05",
                    "updated": "2016-05-28 16:49:05"
                }
            },
            {
                "id": "39",
                "user_id": "1056",
                "discussion_id": "21",
                "comment": "correction parishers",
                "date": "2016-05-29 01:40:32",
                "User": {
                    "id": "1056",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Rogers29761",
                    "last_name": "",
                    "email": "Aliciadillon65@gail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-28 16:49:05",
                    "updated": "2016-05-28 16:49:05"
                }
            },
            {
                "id": "38",
                "user_id": "1056",
                "discussion_id": "21",
                "comment": "parishes wearing mini skirts and dresses at mass",
                "date": "2016-05-29 01:37:52",
                "User": {
                    "id": "1056",
                    "role_id": "3",
                    "device_token": "",
                    "device_type": "",
                    "first_name": "Rogers29761",
                    "last_name": "",
                    "email": "Aliciadillon65@gail.com",
                    "password": "",
                    "image": "",
                    "address": "",
                    "phone": "",
                    "lat": "0",
                    "lon": "0",
                    "status": true,
                    "is_deleted": false,
                    "created": "2016-05-28 16:49:05",
                    "updated": "2016-05-28 16:49:05"
                }
            }
        ]
    }
}